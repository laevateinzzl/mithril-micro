package handlers

import (
	"context"

	pb "mithril-micro/pb/user"
)

// NewService returns a naïve, stateless implementation of Service.
func NewService() pb.UserServiceServer {
	return userserviceService{}
}

type userserviceService struct{}

func (s userserviceService) CreateUser(ctx context.Context, in *pb.CreateUserReq) (*pb.User, error) {
	var resp pb.User
	return &resp, nil
}

func (s userserviceService) FindUser(ctx context.Context, in *pb.FindUserReq) (*pb.User, error) {
	var resp pb.User
	return &resp, nil
}

func (s userserviceService) GetUser(ctx context.Context, in *pb.GetUserReq) (*pb.GetUserRes, error) {
	var resp pb.GetUserRes
	return &resp, nil
}
