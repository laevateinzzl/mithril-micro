module mithril-micro

go 1.16

require (
	github.com/go-kit/kit v0.11.0
	github.com/go-kit/log v0.1.0
	github.com/gogo/protobuf v1.3.2
	github.com/gorilla/mux v1.8.0
	github.com/jesseduffield/lazygit v0.28.2 // indirect
	github.com/pkg/errors v0.9.1
	golang.org/x/text v0.3.6 // indirect
	google.golang.org/grpc v1.39.0
)
