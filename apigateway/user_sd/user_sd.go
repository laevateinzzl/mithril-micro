package user_srv

import (
	"context"
	"io"
	pb_user "mithril-micro/pb/user"
	"mithril-micro/usr_srv/svc"
	g "mithril-micro/usr_srv/svc/client/grpc"
	"time"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/etcdv3"
	"github.com/go-kit/kit/sd/lb"
	"github.com/go-kit/log"
	"google.golang.org/grpc"
)

type UserAgent struct {
	instancerm *etcdv3.Instancer
	logger     log.Logger
}

func NewUserAgentClient(addr []string, logger log.Logger) (*UserAgent, error) {
	var (
		etcdAddrs  = addr
		serverName = "svc.user.agent"
		ttl        = 5 * time.Second
	)

	options := etcdv3.ClientOptions{
		DialTimeout:   ttl,
		DialKeepAlive: ttl,
	}
	etcdClient, err := etcdv3.NewClient(context.Background(), etcdAddrs, options)
	if err != nil {
		return nil, err
	}
	instancerm, err := etcdv3.NewInstancer(etcdClient, serverName, logger)
	if err != nil {
		return nil, err
	}
	return &UserAgent{
		instancerm: instancerm,
		logger:     logger,
	}, err

}

func (user *UserAgent) UserAgentClient() (svc.Endpoints, error) {
	var (
		retryMax     = 3
		retryTimeout = 300 * time.Millisecond
		endpoints    svc.Endpoints
	)

	factory := user.factoryFor(svc.MakeCreateUserEndpoint)
	endpointer := sd.NewEndpointer(user.instancerm, factory, user.logger)
	balancer := lb.NewRandom(endpointer, time.Now().UnixNano())
	retry := lb.Retry(retryMax, retryTimeout, balancer)
	endpoints.CreateUserEndpoint = retry

	return endpoints, nil

}

func (user *UserAgent) factoryFor(makeEndpoint func(pb_user.UserServiceServer) endpoint.Endpoint) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		conn, err := grpc.Dial(instance, grpc.WithInsecure())
		if err != nil {
			return nil, nil, err
		}
		srv, err := g.New(conn)
		endpoints := makeEndpoint(srv)
		return endpoints, conn, err
	}
}
