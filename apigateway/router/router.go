/*
 * @Author: zzl
 * @Date: 2021-07-07 09:52:48
 * @LastEditTime: 2021-07-07 14:54:56
 * @LastEditors: zzl
 * @Description:
 * @FilePath: /mithril-micro/apigateway/router.go
 */
package apigateway

import (
	"mithril-micro/usr_srv/svc"
	"net/http"

	"github.com/gorilla/mux"
)

func InitRouter() *mux.Router {
	router := mux.NewRouter()
	var user svc.Endpoints
	router.PathPrefix("/user").Handler(http.StripPrefix("/user", svc.MakeHTTPHandler(user)))
	return router
}
