/*
 * @Author: zzl
 * @Date: 2021-07-05 15:05:42
 * @LastEditTime: 2021-07-07 14:58:38
 * @LastEditors: zzl
 * @Description:
 * @FilePath: /mithril-micro/apigateway/main.go
 */
package main

import (
	"context"
	"flag"
	"fmt"
	apigateway "mithril-micro/apigateway/router"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd/etcdv3"
)

func main() {
	httpAddr := flag.String("http.addr", ":8000", "Address for HTTP (JSON) server")
	var logger log.Logger

	ttl := time.Second * 10
	options := etcdv3.ClientOptions{
		DialTimeout:   ttl,
		DialKeepAlive: ttl,
	}
	var etcdAddrs = []string{}
	etcdClient, err := etcdv3.NewClient(context.Background(), etcdAddrs, options)
	if err != nil {
		panic(err)
	}

	serverName := "user_srv"
	grpcAddr := ""

	Registar := etcdv3.NewRegistrar(etcdClient, etcdv3.Service{
		Key:   fmt.Sprintf("%s/%s", serverName, grpcAddr),
		Value: grpcAddr,
	}, log.NewNopLogger())

	defer func() {
		Registar.Deregister()
	}()
	Registar.Register()
	r := apigateway.InitRouter()
	// Interrupt handler.
	errc := make(chan error)
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errc <- fmt.Errorf("%s", <-c)
	}()

	// HTTP transport.
	go func() {
		logger.Log("transport", "HTTP", "addr", *httpAddr)
		errc <- http.ListenAndServe(*httpAddr, r)
	}()

	// Run!
	logger.Log("exit", <-errc)
}
